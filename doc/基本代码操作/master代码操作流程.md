# master分支操作流程
```bash
repo init -u git@gitee.com:openharmony/manifest.git -b master --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'    # 下载二进制文件
bash build/prebuilts_download.sh  # 下载编译工具包
./build.sh --product-name rk3568 --ccache
./build.sh --product-name rk3568 --build-target make_test
```